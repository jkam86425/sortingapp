# Sorting App

Maven-based Project, Sorting App is a small Java application that takes up to ten command-line arguments as integer values, sorts them in the ascending order, and then prints them into standard output.

## Instructions

- Take from command line an Integer Value
- If the entered value is not an integer, the previously entered values will be taken, and the sorting process will be performed, with the results printed.