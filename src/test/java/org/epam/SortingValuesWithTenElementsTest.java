package org.epam;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;

@RunWith(Parameterized.class)
public class SortingValuesWithTenElementsTest {
    private Object[] inputs;
    private Object[] expected;

    @Rule
    public final TextFromStandardInputStream systemIn = emptyStandardInputStream();

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    public SortingValuesWithTenElementsTest(Object[] inputs, Object[] expected) {
        this.inputs = inputs;
        this.expected = expected;
    }
    @Test
    public void testSortingValuesWithTenElements() {
        systemIn.provideLines(
                inputs[0].toString(), inputs[1].toString(), inputs[2].toString(),
                inputs[3].toString(), inputs[4].toString(), inputs[5].toString(),
                inputs[6].toString(), inputs[7].toString(), inputs[8].toString(), inputs[9].toString()
        );
        App.main(new String[]{});
        String output = systemOutRule.getLogWithNormalizedLineSeparator().trim().split("\\r?\\n")[1];
        assertEquals(expected[0], output);
    }
    @Parameterized.Parameters
    public static Collection<Object[][]> data() {
        return Arrays.asList(new Object[][][]{
                {{3, 2, 1, 0, 4, 5, 6, 7, 8, 9}, {"[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]"}},
                {{-3, -2, -1, 0, 46, -5, 60, 72, -8, -9}, {"[-9, -8, -5, -3, -2, -1, 0, 46, 60, 72]"}},
                {{1099, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {"[0, 0, 0, 0, 0, 0, 0, 0, 0, 1099]"}},
                {{-3, -2, -1, 0, -4, -5, -6, -7, -8, -9}, {"[-9, -8, -7, -6, -5, -4, -3, -2, -1, 0]"}},
                {{88, 342, 1657, 23, 2, 6, 6, 7, 7, 435}, {"[2, 6, 6, 7, 7, 23, 88, 342, 435, 1657]"}}
        });
    }
}
