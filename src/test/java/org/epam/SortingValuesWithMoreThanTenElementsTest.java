package org.epam;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;

@RunWith(Parameterized.class)
public class SortingValuesWithMoreThanTenElementsTest {
    private Object[] inputs;
    private Object[] expected;

    @Rule
    public final TextFromStandardInputStream systemIn = emptyStandardInputStream();

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    public SortingValuesWithMoreThanTenElementsTest(Object[] inputs, Object[] expected) {
        this.inputs = inputs;
        this.expected = expected;
    }
    @Test
    public void testSortingValuesWithTenElements() {
        systemIn.provideLines(
                inputs[0].toString(), inputs[1].toString(), inputs[2].toString(),
                inputs[3].toString(), inputs[4].toString(), inputs[5].toString(),
                inputs[6].toString(), inputs[7].toString(), inputs[8].toString(),
                inputs[9].toString(), inputs[10].toString(), inputs[11].toString(),
                inputs[12].toString(), inputs[13].toString(), inputs[14].toString()
        );
        App.main(new String[]{});
        String output = systemOutRule.getLogWithNormalizedLineSeparator().trim().split("\\r?\\n")[1];
        assertEquals(expected[0], output);
    }
    @Parameterized.Parameters
    public static Collection<Object[][]> data() {
        return Arrays.asList(new Object[][][]{
                {{3, 2, 1, 0, 4, 5, 6, 7, 8, 9, 11, -8, 56, 56, -888}, {"[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]"}},
                {{-3, 222, -1, 0, -4, -5, 1001, 234, 88, 101, -11, "", "", "", ""}, {"[-5, -4, -3, -1, 0, 88, 101, 222, 234, 1001]"}},
                {{3, 7, 12, 18, 25, 31, 42, 50, 57, 64, 73, 88, "", "", ""}, {"[3, 7, 12, 18, 25, 31, 42, 50, 57, 64]"}},
                {{2, 10, 17, 23, 30, 47, 55, 63, 70, 79, 85, 97, "", "", ""}, {"[2, 10, 17, 23, 30, 47, 55, 63, 70, 79]"}},
                {{9, -15, 22, -28, 36, -41, 53, -60, 67, -74, 81, -92, 7, 74, 0}, {"[-74, -60, -41, -28, -15, 9, 22, 36, 53, 67]"}}
        });
    }
}
