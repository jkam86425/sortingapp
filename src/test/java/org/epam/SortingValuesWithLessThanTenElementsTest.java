package org.epam;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;

/**
 * Unit test for Sorting App.
 */
@RunWith(Parameterized.class)
public class SortingValuesWithLessThanTenElementsTest {

    private Object[] inputs;
    private Object[] expected;

    @Rule
    public final TextFromStandardInputStream systemIn = emptyStandardInputStream();

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    public SortingValuesWithLessThanTenElementsTest(Object[] inputs, Object[] expected) {
        this.inputs = inputs;
        this.expected = expected;
    }
    @Test
    public void testSortingValuesWithLessThanTenElements() {
        systemIn.provideLines(
                inputs[0].toString(), inputs[1].toString(), inputs[2].toString(),
                inputs[3].toString(), inputs[4].toString(), inputs[5].toString(),
                inputs[6].toString(), inputs[7].toString(), inputs[8].toString(), inputs[9].toString()
        );
        App.main(new String[]{});
        String output = systemOutRule.getLogWithNormalizedLineSeparator().trim().split("\\r?\\n")[1];
        assertEquals(expected[0], output);
    }
    @Parameterized.Parameters
    public static Collection<Object[][]> data() {
        return Arrays.asList(new Object[][][]{
                {{3, 2, 1,"", "", "", "", "", "", "", ""}, {"[1, 2, 3]"}},
                {{"", 2, 1, 5, 4, 6, 7, 8, 9, 10}, {"[]"}},
                {{-58, 0, 6, -232, 4, "", "", "", "", ""}, {"[-232, -58, 0, 4, 6]"}},
                {{"", "", "", "", "", "", "", "", "", ""}, {"[]"}},
                {{-58, "", "", "", "", "", "", "", "", ""}, {"[-58]"}}
        });
    }
}
