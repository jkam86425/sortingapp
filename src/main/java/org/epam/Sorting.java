package org.epam;

/**
 * Class that receive an array and sort it
 */
public class Sorting {
    public void sort(int[] arr) {
        if(arr != null){
            int i, j, temp;
            int n = arr.length;
            boolean swapped;
            for (i = 0; i < n - 1; i++) {
                swapped = false;
                for (j = 0; j < n - i - 1; j++) {
                    if (arr[j] > arr[j + 1]) {
                        temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                        swapped = true;
                    }
                }

                if (swapped == false)
                    break;
            }
        } else {
            throw new IllegalArgumentException();
        }
    }
}
