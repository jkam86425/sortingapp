package org.epam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Main class that receive from Standard Input up to 10 values and sort them
 */
public class App {

    static Sorting sorting = new Sorting();

    public static void main(String[] args) {
        int[] values = new int[10];
        int n = 0;
        Scanner scanner = new Scanner(System.in);
        //BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please enter your data below: (send '' to exit) ");
        while (n < 10) {
            try {
                int value = Integer.parseInt(scanner.nextLine());
                values[n] = value;
                n++;
            } catch (NumberFormatException numberFormatException) {
                break;
            } catch (NoSuchElementException noSuchElementException) {
                break;
            }
        }
        int[] arr = new int[n];
        arr = (n == 9) ? values : Arrays.copyOfRange(values, 0, n);
        sorting.sort(arr);
        String output = Arrays.toString(arr);
        System.out.println(output);
    }
}

